DROP TABLE IF EXISTS tutorials;

CREATE TABLE tutorials (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  description VARCHAR(250) NOT NULL,
  published BOOLEAN NOT NULL
);

INSERT INTO tutorials (description,published) VALUES
  ('tuto1',true),
  ('tuto2',true),
  ('tuto3',true);