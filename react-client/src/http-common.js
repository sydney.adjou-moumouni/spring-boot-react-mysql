import axios from "axios";

export default axios.create({
  baseURL: "https://kaibee-gitlab-backend.herokuapp.com/api/",
  headers: {
    "Content-type": "application/json"
  }
});
